/** 
 * Script to control Jeroen's car lights
 *  
 * - Direction is always from when you look at the car.
 */
const bool DEBUG_TICKER = false;
const bool DEBUG_BLINKER = false;
const bool DEBUG_IGNITION = false;

const int UNSET = -1;

const int MAX_SIDES = 2; // Total number of sides
const int MAX_LIGHTS = 3; // For each side

const int PIN_LIGHTS[MAX_SIDES][MAX_LIGHTS] = {
  {
    3,  // Outer left
    5,  // Middel left
    6   // Inner left
  },
  {
    9, // Outer right
    10, // Middel right
    11   // Inner right
  }
};

const int PIN_BLINK[MAX_SIDES] = {
  2, // Left
  12 // Right
};

const int PIN_DIM = 8;

const int INITIAL_BRIGHTNESS[MAX_SIDES][MAX_LIGHTS] = {
  {
    0,    // Outer left
    -40,  // Middel left
    -80   // Inner left
  },
  {
    0,    // Outer right
    -40,  // Middel right
    -80   // Inner right
  }
};

const int LOCK_BRIGHTNESS[MAX_SIDES][MAX_LIGHTS] = {
  {
    180,    // Outer left
    140,  // Middel left
    100   // Inner left
  },
  {
    180,    // Outer right
    140,  // Middel right
    100   // Inner right
  }
};

const int MIN_BRIGHTNESS = 0; // Min brightness all the lights
const int MAX_BRIGHTNESS = 100; // Max brightness all the lights in normal operation
const int MAX_BRIGHTNESS_DIM = 10; // For all lights while dimming lights are on
const int MAX_BRIGHTNESS_IGNITION = 0; // For all lights when the ignition is NOT turned on after IGNITION_DELAY
const int MAX_BRIGHTNESS_BLINK_UP = 10; // Force all light to this brightness when going up

const int BRIGHTNESS_LOCK = 50; // Force all lights to this state when locking

const int IGNITION_DELAY = 0; // Time in ms for the ignition to be off, after the lights turn off
const int IGNITION_BLINKER = 1; // Which blinker shall we watch for the ignition?

// For the outer and inner light, middel light always zero
const int MAX_BRIGHTNESS_BLINK[] = { 
  10,
  0,
  10 
};

const int SPEED_DEFAULT = 35; // Default animatin speed (boot)
const int SPEED_DOWN = 1; // When we animate the lights off
const int SPEED_UP = 25; // When we animate the lights on
const int SPEED_LOCK = 250; // When we animate the lights lock (go out)


const int STATE_LOCKED = 0; // Car is locked.
const int STATE_UNLOCKING = 1; // Car is unlocking (after first blink is detected)
const int STATE_UNLOCKING2 = 2; // Car is unlocking (after second blink is detected)
const int STATE_UNLOCKED = 3; // Car is unlocked (after 2 blinks has been done)
const int STATE_LOCKING = 4; // Long blink detected, going to sleep after IGNITION_DELAY.

const int BLINK_DELAY = 800;// Delay in ms after last blink
const int BLINK_STOP = 900; // How long a blink should atleast take to trigger an ignition stop

// Defaults to the startup state.
int currentLightBrightness[MAX_SIDES][MAX_LIGHTS] = {
  {
    MIN_BRIGHTNESS, // Outer left
    MIN_BRIGHTNESS, // Middel left
    MIN_BRIGHTNESS  // Inner left
  },
  {
    MIN_BRIGHTNESS, // Outer right
    MIN_BRIGHTNESS, // Middel right
    MIN_BRIGHTNESS  // Inner right
  }
};

// Target lights brightness to work to 
int targetLightBrightness[MAX_SIDES][MAX_LIGHTS] = {
  {
    MIN_BRIGHTNESS, // Outer left
    MIN_BRIGHTNESS, // Middel left
    MIN_BRIGHTNESS  // Inner left
  },
  {
    MIN_BRIGHTNESS, // Outer right
    MIN_BRIGHTNESS, // Middel right
    MIN_BRIGHTNESS  // Inner right
  }
};

// Minimum lights brightness to work from
int minimumLightBrightness[MAX_SIDES][MAX_LIGHTS] = {
  {
    MIN_BRIGHTNESS, // Outer left
    MIN_BRIGHTNESS, // Middel left
    MIN_BRIGHTNESS  // Inner left
  },
  {
    MIN_BRIGHTNESS, // Outer right
    MIN_BRIGHTNESS, // Middel right
    MIN_BRIGHTNESS  // Inner right
  }
};

// The last time the animate ticked
int sideAnimateTick[MAX_SIDES] = {
  0, // Left
  0  // Right
};

// The speed of the animation for the lights
int sideAnimateSpeed[MAX_SIDES] = {
  SPEED_DEFAULT, // Left
  SPEED_DEFAULT  // Right
};

bool isDimmed = false;
int isBlinking[] = {
  UNSET, // Left
  UNSET  // Right
};
int startBlinking[] = {
  UNSET, // Left
  UNSET  // Right
};

int lastIgnition = UNSET;

int lockState = STATE_LOCKED;

void setup() {
  if (DEBUG_BLINKER || DEBUG_TICKER || DEBUG_IGNITION) {
    Serial.begin(2000000);
  }
  
  digitalWrite(PIN_DIM, LOW);
  
  for (int s = 0; s < MAX_SIDES; s++) {
    digitalWrite(PIN_BLINK[s], LOW);

    for (int l = 0; l < MAX_LIGHTS; l++) {
      pinMode(PIN_LIGHTS[s][l], OUTPUT);
      analogWrite(PIN_LIGHTS[s][l], 0);      
    }
  }
}

void loop() {
  int current = millis();
  
  bool isCurrentlyBlinking[MAX_SIDES];

  for (int s = 0; s < MAX_SIDES; s++) {
    isCurrentlyBlinking[s] = digitalRead(PIN_BLINK[s]) == HIGH;
  }
  
  handleDim(current);
  handleIgnition(current, isCurrentlyBlinking[IGNITION_BLINKER]);  

  for (int s = 0; s < MAX_SIDES; s++) {
    handleBlink(current, s, isCurrentlyBlinking[s]);
  }
  
  tick(current);
}

void handleIgnition(int current, bool isCurrentlyBlinking) {
  if (DEBUG_IGNITION) {
//    Serial.print("Ignition state: ");
//    Serial.println(lockState);
  }

  if (isCurrentlyBlinking) {
    if (DEBUG_IGNITION) {
//      Serial.print("Ignition blinker state: ");
//      Serial.println(isBlinking[IGNITION_BLINKER]);
    }
    
    if (startBlinking[IGNITION_BLINKER] == UNSET) {
    
      if (lockState == STATE_UNLOCKING) {
        // Car was unlocking after first blink, second blink detected.
        setLockState(STATE_UNLOCKING2);
        
        if (DEBUG_IGNITION) {
          Serial.println("Another blink detected, going in second stage unlock..");
        }
      }
      
      if (lockState == STATE_LOCKED || lockState == STATE_LOCKING) {
        // Car was locked, going to unlock..
        setLockState(STATE_UNLOCKING);
        lastIgnition = UNSET;
        
        if (DEBUG_IGNITION) {
          Serial.println("Blink detected, going in unlock..");
        }
      }
             
      return;
    }

    return;
  }
    
  if (startBlinking[IGNITION_BLINKER] == UNSET) {
    if (lockState == STATE_UNLOCKING2) {
      // Car was unlocking, starting startup procedure..
      setLockState(STATE_UNLOCKED);
      
      if (DEBUG_IGNITION) {
        Serial.println("Last blink gone down, car unlocked.");
      }
      
      for (int s = 0; s < MAX_SIDES; s++) {    
        sideAnimateSpeed[s] = SPEED_DEFAULT;
        
        for (int l = 0; l < MAX_LIGHTS; l++) {
          if (!isDimmed) {
            minimumLightBrightness[s][l] = currentLightBrightness[s][l];
            currentLightBrightness[s][l] = INITIAL_BRIGHTNESS[s][l] + currentLightBrightness[s][l];
          }
          
          targetLightBrightness[s][l] = isDimmed ? MAX_BRIGHTNESS_DIM : MAX_BRIGHTNESS;   
        }
      }
      
      return;
    }
  }

  if (lastIgnition == UNSET) {
    return;
  }

  if ((current - lastIgnition) < IGNITION_DELAY) {
    return;
  }
  
  if (DEBUG_IGNITION) {
    Serial.println("Car locked, turning everything off.");
  }
  
  for (int s = 0; s < MAX_SIDES; s++) {
    sideAnimateSpeed[s] = SPEED_DEFAULT;
    
    for (int l = 0; l < MAX_LIGHTS; l++) {
      currentLightBrightness[s][l] = LOCK_BRIGHTNESS[s][l]; 
      targetLightBrightness[s][l] = MAX_BRIGHTNESS_IGNITION; 
    }
  }

  setLockState(STATE_LOCKED);
  lastIgnition = UNSET;
}

void handleDim(int current) {
  bool isCurrentlyDimmed = digitalRead(PIN_DIM) == HIGH;
  
  if (isCurrentlyDimmed == isDimmed) {
    return;
  }

  isDimmed = isCurrentlyDimmed;
    
  if (isCurrentlyDimmed) {
    // Currently dimmed, so decrease brightness 
    for (int s = 0; s < MAX_SIDES; s++) {
      if (isBlinking[s] != UNSET) {
        continue;
      }
      
      sideAnimateSpeed[s] = SPEED_DOWN;
      
      for (int l = 0; l < MAX_LIGHTS; l++) {
        targetLightBrightness[s][l] = MAX_BRIGHTNESS_DIM;   
      }
    }
    
    return;
  }
  
  // Currently NOT dimmed, so increase brightness 
  for (int s = 0; s < MAX_SIDES; s++) {
    if (isBlinking[s] != UNSET) {
      continue;
    }
    
    sideAnimateSpeed[s] = SPEED_UP;
    
    for (int l = 0; l < MAX_LIGHTS; l++) {
      targetLightBrightness[s][l] = MAX_BRIGHTNESS;   
    }
  }
}

void handleBlink(int current, int s, bool isCurrentlyBlinking) {
  if (isCurrentlyBlinking) {    
    if (isBlinking[s] == UNSET) {
      // Was not bliking but is now, so decrease brightness
     
      if (DEBUG_BLINKER) {
        Serial.println("Started blinking");
      }

      // Set when current blink is started
      setStartBlinking(s, current);

      if (lockState == STATE_UNLOCKED) {
        sideAnimateSpeed[s] = SPEED_DOWN;
        
        for (int l = 0; l < MAX_LIGHTS; l++) {
          targetLightBrightness[s][l] = MAX_BRIGHTNESS_BLINK[l];   
        }
      }
    } 
    
    setIsBlinking(s, current);

    if (DEBUG_BLINKER) {
      //Serial.println("Still blinking");
    }
    
    return;
  }

  if (isBlinking[s] == UNSET) {
    if (DEBUG_BLINKER) {
      //Serial.println("No blink signal detected and not blinking.");
    }
    
    return;
  }
  
  if (s == IGNITION_BLINKER) {
    if (DEBUG_IGNITION) {
      Serial.print("Started blinking: ");
      Serial.print(startBlinking[IGNITION_BLINKER]);
      Serial.print(" - ");
      Serial.println(current - startBlinking[IGNITION_BLINKER]);
    }
    
    if (lockState == STATE_UNLOCKED && startBlinking[IGNITION_BLINKER] != UNSET && (current - startBlinking[IGNITION_BLINKER]) >= BLINK_STOP) {
      // Blink was longer then normal, stopping ignition.
      setLockState(STATE_LOCKING);
      lastIgnition = current;
      
      if (DEBUG_IGNITION) {
        Serial.println("Car locking.");
      }
  
      for (int s = 0; s < MAX_SIDES; s++) {
        for (int l = 0; l < MAX_LIGHTS; l++) {
          currentLightBrightness[s][l] = BRIGHTNESS_LOCK;
        }
      }
      return;
    }
  }
  
  setStartBlinking(s, UNSET);
  
  if (lockState != STATE_UNLOCKED) {
    setIsBlinking(s, UNSET);
    return;
  }
  
  if ((current - isBlinking[s]) < BLINK_DELAY) {
    if (DEBUG_BLINKER) {
      Serial.print("Currently blinking, no time yet to increase brightbess (");
      Serial.print(current - isBlinking[s]);
      Serial.print("ms not reached ");
      Serial.print(BLINK_DELAY);
      Serial.println("ms yet)");
    }
    
    return;
  }

  if (DEBUG_BLINKER) {
    Serial.println("Was blinking, but time expired increasing brightness!");
  }

  // Was blinking for BLINK_DELAY, so increase brightness
  sideAnimateSpeed[s] = isDimmed ? SPEED_DOWN : SPEED_UP;
  
  for (int l = 0; l < MAX_LIGHTS; l++) {
    currentLightBrightness[s][l] = MAX_BRIGHTNESS_BLINK_UP;
    targetLightBrightness[s][l] = isDimmed ? MAX_BRIGHTNESS_DIM : MAX_BRIGHTNESS;   
  }

  setIsBlinking(s, UNSET);
}

void tick(int current) {  
  // Bring up the brightness to the target brightness
  for (int s = 0; s < MAX_SIDES; s++) {
    if ((current - sideAnimateTick[s]) < sideAnimateSpeed[s]) {
      continue;
    }

    sideAnimateTick[s] = current;
    
    if (DEBUG_TICKER) {
      Serial.print(s);
      Serial.print(" | ");
    }
        
    for (int l = 0; l < MAX_LIGHTS; l++) {   
      if (currentLightBrightness[s][l] != targetLightBrightness[s][l]) {
        if (currentLightBrightness[s][l] < targetLightBrightness[s][l]) {
          currentLightBrightness[s][l]++;
        } else {
          currentLightBrightness[s][l]--; 
        }
      }

      if (targetLightBrightness[s][l] < minimumLightBrightness[s][l]) {
        minimumLightBrightness[s][l] = targetLightBrightness[s][l];
      }

      int brightness = currentLightBrightness[s][l];

      if (brightness < minimumLightBrightness[s][l]) {
        brightness = minimumLightBrightness[s][l];
      } else {
        minimumLightBrightness[s][l] = MIN_BRIGHTNESS;
      }
      
      if (brightness > MAX_BRIGHTNESS) {
        brightness = MAX_BRIGHTNESS;
      }
      
      analogWrite(PIN_LIGHTS[s][l], brightness);    

     
      if (DEBUG_TICKER) {
        Serial.print(l);
        Serial.print(" = ");
        Serial.print(brightness);
        Serial.print(" (");
        Serial.print(minimumLightBrightness[s][l]);
        Serial.print(" - ");
        Serial.print(targetLightBrightness[s][l]);
        Serial.print(") - ");
      }
    }
  
    if (DEBUG_TICKER) {
      Serial.println();
    }
  }
}

void setLockState(int state) {
  if (state != lockState) {
    Serial.print("Setting lock state to: ");
    Serial.println(state);
  }

  lockState = state;
}


void setIsBlinking(int s, int state) {
  if (state != isBlinking[s]) {
//    Serial.print("Setting is blinking side ");
//    Serial.print(s);
//    Serial.print(" to: ");
//    Serial.println(state);
  }

  isBlinking[s] = state;
}


void setStartBlinking(int s, int state) {
  if (state != startBlinking[s]) {
    Serial.print("Setting start blinking side ");
    Serial.print(s);
    Serial.print(" to: ");
    Serial.println(state);
  }

  startBlinking[s] = state;
}
